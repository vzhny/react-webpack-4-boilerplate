import React from 'react';

const NotFound = () => {
  const styles = {
    textAlign: 'center',
    color: '#333',
    fontSize: '4rem',
    fontWeight: '100',
    margin: '0',
  };

  return (
    <>
      <h1 style={styles}>404 Not Found</h1>
    </>
  );
};

export default NotFound;
